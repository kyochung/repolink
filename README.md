# Repolink

Será una plataforma web integrada a la página oficial de ProinChile, que tiene como objetivo almacenar links recomendados por integrantes de la comunidad, e invitar a más personas a registrarse para compartir conocimiento con la comunidad.  

## Funcionalidades

- Buscador de link por medio de tags, al estilo pinterest.
- Los links serán clasificados por: 
	* Tutoriales
	* Libros
	* Blogs
	* Aplicaciones
- Links publicados podrán ser valorados con un punto positivo o negativo, y comentados por cada usuario.
- Contará con una url corta para compartir en las redes sociales.
- En la página principal se mostrará una lista de links con un máximo de 10, bajo los siguientes criterios:
	+ Links subidos (por el usuario)
	+ Links party (con mayor actividad durante la semana)
	+ Links top 10 (más votos positivos durante el mes)
	+ Links recomendados (recomendados por administradores de la comunidad)
	+ Links nuevos 	(últimos publicados)

## Etapas de desarrollo:

- Definición de funcionalidades
- Diseño de UX/UI 
- Definición de herramientas y metodología de desarrollo
- Diseño de BD
- Maquetado
- Desarrollo de prototipo funcional
- Testeo
- Lanzamiento